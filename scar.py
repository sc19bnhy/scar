import os
import re
import time
import json
import pickle
from nltk.util import ngrams
import pyarabic.araby as araby
from collections import defaultdict


# Set some constants for the spellchecker
max_distance = 1  # Maximum edit distance for spell correction
min_frequency = 1  # Minimum frequency of a word in the corpus for it to be considered

# Normalize Arabic text (remove diacritics, replace similar characters, etc.)
def normalize_text(input):

    # Define Arabic letters and characters to keep in the normalized text
    letters = "ابتةثجحخدذرزسشصضطظعغفقكلمنهويءآأإؤىئ \n"

    # Remove diacritics from the input text
    input = araby.strip_diacritics(input)

    # Replace similar characters
    input = re.sub("[آأإ]", "ا", input)
    input = re.sub("ئ", "ي", input)
    input = re.sub("ؤ", "و", input)

    # Remove any characters that are not in the defined Arabic letters
    input = "".join(c for c in input if c in letters)

    # Remove extra consecutive spaces while keeping new line
    input = re.sub(r" +", " ", input)

    return input

# Load word frequencies from a file
def load_word_frequencies(filename):
    word_freq = {}

    # Open word frequencies file
    with open(filename, "r", encoding="utf-8") as file:

        for line in file:

            # Load the words and their frequencies
            word, freq = line.strip().split(":")
            freq = int(freq)

            # Load word if it appears more than specified frequency
            # and if it has more than 1 letter
            if freq >= min_frequency and len(word) >= 2:
                word_freq[word] = freq

    return word_freq

# Modified Levenshtein distance function with customizable costs
def modified_levenshtein_distance(s1, s2, cost_insert=1, cost_delete=1, cost_substitute=1, cost_transpose=1):

    # If word edit distance is over maximum distance specified do not allow
    if abs(len(s1) - len(s2)) > max_distance:
        return float("inf")
    
    # If s1 is shorter than s2, swap the two strings
    if len(s1) < len(s2):
        return modified_levenshtein_distance(s2, s1)

    # If s2 is empty return s1
    if len(s2) == 0:
        return len(s1)

    # Initialize a list representing the previous row of distances with values from 0 to the length of s2
    previous_row = range(len(s2) + 1)

    for i, c1 in enumerate(s1):

        # Initialize a list representing the current row of distances with the first element as i + 1
        current_row = [i + 1]

        for j, c2 in enumerate(s2):

            # Calculate the costs of insertion, deletion, and substitution based on the specified costs
            insertions = previous_row[j + 1] + cost_insert 
            deletions = current_row[j] + cost_delete       
            substitutions = previous_row[j] + (c1 != c2) * cost_substitute
            
            # Calculate the cost of transposition or set it to infinity if not possible
            if i > 0 and j > 0 and s1[i] == s2[j - 1] and s1[i - 1] == s2[j]:
                transpositions = previous_row[j - 1] + cost_transpose
            else:
                transpositions = float("inf")

            # Calculate and append the minimum cost among insertion, deletion, substitution, and transposition
            current_row.append(min(insertions, deletions, substitutions, transpositions))

        # Update the previous row to be the current row for the next iteration
        previous_row = current_row

    # Return the final value in the last row of the matrix, which represents the modified Levenshtein distance
    return previous_row[-1]

# Find candidate words for a misspelled word
def find_candidate_words(input_word, dictionary, word_freq, total_word_count, top_n=5):

    # If the input word is less than 2 characters, return it as a candidate with a distance of 0
    if len(input_word) < 2:
        return [(input_word, 0, word_freq.get(input_word, 0) / total_word_count)]

    # If the input word is found in dictionary, return it as a candidate with a distance of 0
    if input_word in dictionary:
        return [(input_word, 0, word_freq.get(input_word, 0) / total_word_count)]

    candidates = []

    for word in dictionary:

        # If word has 2 or more characters
        if len(word) >= 2:

            # Calculate the modified Levenshtein distance between the input word and each dictionary word
            distance = modified_levenshtein_distance(input_word, word)

            if distance <= max_distance:

                # Calculate the MLE of the dictionary word's frequency
                mle = word_freq.get(word, 0) / total_word_count

                # Add the dictionary word, its distance, and its MLE as a candidate
                candidates.append((word, distance, mle))

    # Sort the candidates first by lowest distance and then by highest MLE
    candidates.sort(key=lambda x: (x[1], -x[2]))

    # Return the top N candidates
    return candidates[:top_n]

# Correct a sentence using edit distance
def edit_distance_correct_sentence(sentence, dictionary, word_freq, total_word_count):
    corrected_words = []

    # Tokenize the input sentence into words
    words = araby.tokenize(sentence)

    for word in words:

        # Find candidate words that are similar to the current word using edit distance
        candidates = find_candidate_words(word, dictionary, word_freq, total_word_count)

        # Select the best candidate word with the highest MLE score
        best_candidate = max(candidates, key=lambda x: x[2])[0] if candidates else word

        # Append the best candidate word to the list of corrected words
        corrected_words.append(best_candidate)

    # Join the corrected words into a sentence and return it
    return " ".join(corrected_words)

# Build n-gram models from a given corpus
def build_ngram_models(corpus):

    # Tokenize the input sentence into words
    words = araby.tokenize(corpus)

    # Create a defaultdict to store frequencies
    unigrams = defaultdict(int)
    bigrams = defaultdict(int)
    trigrams = defaultdict(int)

    # Count the frequencies for each word in
    # unigram, bigram, and trigram
    for word in words:
        unigrams[word] += 1
    for bigram in ngrams(words, 2):
        bigrams[bigram] += 1
    for trigram in ngrams(words, 3):
        trigrams[trigram] += 1

    # Calculate the vocabulary size (unique words)
    vocabulary_size = len(unigrams)

    return unigrams, bigrams, trigrams, vocabulary_size

# Load or create n-gram models from a corpus file
def load_or_create_ngram_models(corpus_file_path):

    # Path to n-gram model file
    ngram_model_file="data/ngram_models.pkl"

    # If the n-gram model file already exists, load it
    if os.path.exists(ngram_model_file):
        
        # Load n-gram models from the file
        with open(ngram_model_file, "rb") as f:
            ngram_models = pickle.load(f)

    # Else create it and save it
    else:
        
        with open(corpus_file_path, "r", encoding="utf-8") as corpus_file:
            arabic_corpus = corpus_file.read()

        # Build n-gram models from the Arabic corpus
        unigram_model, bigram_model, trigram_model, vocabulary_size = build_ngram_models(arabic_corpus)

        # Create dictionary to store n-gram models and vocabulary size
        ngram_models = {
            "unigrams": unigram_model,
            "bigrams": bigram_model,
            "trigrams": trigram_model,
            "vocabulary_size": vocabulary_size
        }

        # Open file to store n-gram models
        with open(ngram_model_file, "wb") as f:
            pickle.dump(ngram_models, f)

    return ngram_models

# Edit distance function for a single word
def edit_distance(word):

    letters = "ابتثجحخدذرزسشصضطظعغفقكلمنهويء"

    # Generate all possible splits of the word into two parts (prefix and suffix)
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]

    # Create a list of words by deleting one character from the word
    deletes = [L + R[1:] for L, R in splits if R]

    # Create a list of words by transposing adjacent characters
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R) > 1]

    # Create a list of words by replacing a character with another character from the specified Arabic letters
    replaces = [L + c + R[1:] for L, R in splits if R for c in letters]

    # Create a list of words by inserting one character into the word from the specified Arabic letters
    inserts = [L + c + R for L, R in splits for c in letters]

    # Combine all generated words from different operations and return them as a set
    return set(deletes + transposes + replaces + inserts)

# Suggest corrections for a misspelled word based on context
def suggest_correction_arabic(word, unigrams, bigrams, trigrams, context, vocabulary_size):
    scores = {}

    # Get a set of candidate words based on edit distance
    candidates = edit_distance(word)

    # Calculate a context-based score for the candidate word using n-gram models
    # Store the candidate word and its score in a list
    for candidate in candidates:

        context_score = estimate_probability(candidate, context, unigrams, bigrams, trigrams, vocabulary_size)
        scores[candidate] = context_score

    # Return the candidate word with the highest context score
    # Use original word as candidate if no candidates are found
    return max(scores, key=scores.get, default=word)

# Estimate the probability of a candidate word
def estimate_probability(candidate, context, unigrams, bigrams, trigrams, vocabulary_size):
    unigram_weight = 1.0
    bigram_weight = 1.0
    trigram_weight = 1.0

    # Calculate unigram probability by adding 1 to the word frequency and dividing by the total word count
    unigram_prob = (unigrams.get(candidate, 0) + 1) / (sum(unigrams.values()) + vocabulary_size)
    
    # Calculate bigram probability using the context word and candidate word frequencies
    bigram_prob = 0
    if context:
        bigram_context = context[-1]
        bigram_prob = (bigrams.get((bigram_context, candidate), 0) + 1) / (unigrams.get(bigram_context, 0) + vocabulary_size)
    
    # Calculate trigram probability using the context of the two previous words and candidate word frequencies
    trigram_prob = 0
    if len(context) > 1:
        trigram_context = (context[-2], context[-1])
        trigram_prob = (trigrams.get((trigram_context[0], trigram_context[1], candidate), 0) + 1) / (bigrams.get((trigram_context[0], trigram_context[1]), 0) + vocabulary_size)
    
    # Combine unigram, bigram, and trigram probabilities with specified weights
    combined_prob = unigram_prob * unigram_weight + bigram_prob * bigram_weight + trigram_prob * trigram_weight

    # Return the estimated combined probability for the candidate word
    return combined_prob

# Correct a sentence using n-gram models
def ngram_correct_sentence(sentence, unigrams, bigrams, trigrams, vocabulary_size):
    spellcheck_text = []

    # Tokenize the input sentence into words
    words = araby.tokenize(sentence)

    for i, word in enumerate(words):

        # Check if the word has a minimum length of 2 characters and its frequency is below the threshold
        # If so, suggest a correction for the word based on its context and n-gram models
        if len(word) >= 2 and unigrams.get(word, 0) < min_frequency:
            context = words[max(0, i-2):i]
            corrected_word = suggest_correction_arabic(word, unigrams, bigrams, trigrams, context, vocabulary_size)

        # Else keep word unchanged if conditions are not met
        else:
            corrected_word = word

        # Add correct word to the corrected sentence
        spellcheck_text.append(corrected_word)

    return " ".join(spellcheck_text)

# Correct a sentence using a combination of edit distance and n-gram models
def combined_correct_sentence(sentence, unigrams, bigrams, trigrams, vocabulary_size, dictionary, word_freq, total_word_count, edit_distance_weight=0.5, ngram_weight=0.5):
    spellcheck_text = []

    # Tokenize the input sentence into words
    words = araby.tokenize(sentence)

    for i, word in enumerate(words):

        # Check if the word has a minimum length of 2 characters and its frequency is below the threshold
        # If so, find the top edit distance candidates for the misspelled word
        if len(word) >= 2 and unigrams.get(word, 0) < min_frequency:
            edit_distance_candidates = find_candidate_words(word, dictionary, word_freq, total_word_count, top_n=10)

            best_score = None
            best_candidate = word

            # Estimate the probability of the candidate word based on n-gram models and context
            # Calculate a combined score using both edit distance and n-gram model scores
            for candidate, distance, _ in edit_distance_candidates:
                ngram_score = estimate_probability(candidate, words[max(0, i-2):i], unigrams, bigrams, trigrams, vocabulary_size)
                combined_score = (edit_distance_weight * (max_distance - distance)) + (ngram_weight * ngram_score)

                # Update the best score if the current candidate is better
                # Update the best candidate as the current candidate
                if best_score is None or combined_score > best_score:
                    best_score = combined_score
                    best_candidate = candidate

            # Set the corrected word as the best candidate
            corrected_word = best_candidate

        # Else keep the word unchanged if it doesn't meet the criteria
        else:
            corrected_word = word

        # Add correct word to the corrected sentence
        spellcheck_text.append(corrected_word)

    return " ".join(spellcheck_text)

# Load JSON data from a file
def load_json_data(file_path):

    with open(file_path, "r", encoding="utf-8") as file:
        return json.load(file)


# Start timer for performance measurement
time_start = time.time()

# Define file paths
corpus_file_path    = "data/2M_ara_corpus_clean.txt"
word_freq_file_path = "data/2M_ara_sorted_word_freq.txt"

#####                                                                #####
##### !!! To adjust MIN_WORD_FREQ & MAX_EDIT_DISTANCE at the top !!! #####
#####                                                                #####

# CHOOSE THE SPELLCHECKING MODEL
# 1 - Edit Distance Model
# 2 - N-Grams Model
# 3 - Combined Model
model = 3 ###
#############

# Load/run necessary files and functions for each model
# Created to save time
if model == 1: # Edit Distance
    word_freq = load_word_frequencies(word_freq_file_path)
    arabic_dictionary = list(word_freq.keys())
    total_word_count = sum(word_freq.values())

elif model == 2: # N-Gram
    ngram_models = load_or_create_ngram_models(corpus_file_path)
    unigram_model = ngram_models["unigrams"]
    bigram_model = ngram_models["bigrams"]
    trigram_model = ngram_models["trigrams"]
    vocabulary_size = ngram_models["vocabulary_size"]

elif model == 3: # Combined
    word_freq = load_word_frequencies(word_freq_file_path)
    arabic_dictionary = list(word_freq.keys())
    total_word_count = sum(word_freq.values())
    ngram_models = load_or_create_ngram_models(corpus_file_path)
    unigram_model = ngram_models["unigrams"]
    bigram_model = ngram_models["bigrams"]
    trigram_model = ngram_models["trigrams"]
    vocabulary_size = ngram_models["vocabulary_size"]    

else:
    print("Please Enter a Correct Number")

# Initialize counters for accuracy calculations
correct_counter = 0
total_counter   = 0

# Loads json file that contains correct and wrong sentences
data = load_json_data("data/benchmark.json")

# Iterate through the benchmark sentences to test spell checker models
for item in data:
    total_counter += 1

    input_text = item["sentence"]
    correct_text = item["correct sentence"]

    # Normalize wrong and correct sentences from json file
    input_text = normalize_text(araby.strip_diacritics(input_text))
    correct_text = normalize_text(araby.strip_diacritics(correct_text))

    # Enter normalized sentence to fix spelling mistakes and then compare to correct sentence
    if model == 1:
        spellcheck_text = edit_distance_correct_sentence(input_text, arabic_dictionary, word_freq, total_word_count)
    elif model == 2:
        spellcheck_text = ngram_correct_sentence(input_text, unigram_model, bigram_model, trigram_model, vocabulary_size)
    elif model == 3:
        spellcheck_text = combined_correct_sentence(input_text, unigram_model, bigram_model, trigram_model, vocabulary_size, arabic_dictionary, word_freq, total_word_count)

    # 
    if spellcheck_text == correct_text:
        correct_counter += 1

    # Debugging to display mismatched words #
    # else:
    #     if len(spellcheck_text) == len(correct_text):

    #         tokenized_input = araby.tokenize(input_text)
    #         tokenized_spellcheck = araby.tokenize(spellcheck_text)
    #         tokenized_correct = araby.tokenize(correct_text)
            
    #         min_length = min(len(tokenized_spellcheck), len(tokenized_correct))

    #         for index in range(min_length):

    #             if tokenized_correct[index] != tokenized_spellcheck[index]:
    #                 print("input:", tokenized_input[index], "spellcheck: ", tokenized_spellcheck[index], ", ", "correct: ", tokenized_correct[index])

# Calculate and print accuracy
print("Accuracy = ", correct_counter, "/", total_counter)
print(" =", correct_counter / total_counter)

# End timer for performance measurement
time_end = time.time()

# Print the elapsed time in seconds
print(round(time_end - time_start, 1), "s")