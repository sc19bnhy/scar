import re
import pyarabic.araby as araby
from collections import Counter
from pyarabic.araby import tokenize, is_arabicrange


# Define file paths for the input data
wiki_100k_file_path = "data/ara_wikipedia_2021_100K-sentences.txt"
news_100k_file_path = "data/ara_news_2022_100K-sentences.txt"
wiki_1m_file_path = "data/ara_wikipedia_2021_1M-sentences.txt"
news_1m_file_path = "data/ara_news_2022_1M-sentences.txt"

# Normalize text before tokenization
def normalize_text(input):

    # Define letters to be keep
    letters = "ابتةثجحخدذرزسشصضطظعغفقكلمنهويءآأإؤىئ \n"

    # Remove diacritics from the input text
    input = araby.strip_diacritics(input)

    # Replace similar characters
    input = re.sub("[آأإ]", "ا", input)
    input = re.sub("ئ", "ي", input)
    input = re.sub("ؤ", "و", input)

    # Remove any characters that are not in the defined Arabic letters
    input = "".join(c for c in input if c in letters)

    # Remove extra consecutive spaces while keeping new line
    input = re.sub(r" +", " ", input)

    return input


# Open and load the contents of the wiki and news files 200k words
with open(wiki_100k_file_path, "r", encoding="utf-8") as wiki_file, open(news_100k_file_path, "r", encoding="utf-8") as news_file:
    input_200k = wiki_file.read() + news_file.read()

normalized_200k = normalize_text(input_200k)

# Tokenize the sentence into words
tokens_200k = tokenize(normalized_200k, conditions=is_arabicrange)

words_list_200k = tokens_200k
word_frequencies_200k = Counter(words_list_200k)
sorted_word_frequencies_200k = word_frequencies_200k.most_common()

with open("data/200K_ara_corpus_clean.txt", "w", encoding="utf-8") as ccfile:
    ccfile.write(normalized_200k)

with open("data/200K_ara_sorted_word_freq.txt", "w", encoding="utf-8") as ffile:
    for word, frequency in sorted_word_frequencies_200k:
        ffile.write(f"{word}: {frequency}\n")


# Open and load the contents of the wiki and news files 2m words
with open(wiki_1m_file_path, "r", encoding="utf-8") as wiki_file, open(news_1m_file_path, "r", encoding="utf-8") as news_file:
    input_2m = wiki_file.read() + news_file.read()
    
normalized_2m   = normalize_text(input_2m)

# Tokenize the sentence into words
tokens_2m   = tokenize(normalized_2m, conditions=is_arabicrange)

words_list_2m = tokens_2m
word_frequencies_2m = Counter(words_list_2m)
sorted_word_frequencies_2m = word_frequencies_2m.most_common()

with open("data/2M_ara_corpus_clean.txt", "w", encoding="utf-8") as ccfile:
    ccfile.write(normalized_2m)

with open("data/2M_ara_sorted_word_freq.txt", "w", encoding="utf-8") as ffile:
    for word, frequency in sorted_word_frequencies_2m:
        ffile.write(f"{word}: {frequency}\n")