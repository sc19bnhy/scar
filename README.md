# SCAR (Spell Checker for the Arabic Language)

## Introduction
SCAR is a spell-checking tool designed for the Arabic language. The spell checker uses different methods to test and standarize the Arabic spell checking process with a combination of natural language processing techniques including edit distance algorithms, n-gram models, and a combined approach, SCAR offers quick and context-aware spell correction. This project is implemented in Python 3.9.

## Installation

### Prerequisites
- Python 3.9
- pip (Python package installer)

### Setting up the Spell Checker
1. **Clone the Repository**: Clone this repository to your local machine or download the source code.
    ```bash
    git clone https://gitlab.com/sc19bnhy/scar.git
    ```
---
2. **Necessary Libraries & Packages**: Install all the required packages using pip.
    - os
    - re
    - json
    - time
    - nltk
    - pickle
    - pyarabic
---
3. **Unzip txt Files** Navigate to Data Folder then unzip all .txt.bz2 files using the terminal or any unzip tool.
---
4. **Run preprocessing.py**:
    - Terminal:
        ```bash
        python preprocessing.py
        ```
    - (Optional) you can use your own corpus by modifying preprocessing.py
---
5. **Run scar.py**:
    - Terminal:
    ```bash
    python scar.py
    ```
    - ATTENTION!: An ngram_models.pkl file will be created in the data folder when you run the code for the first time. This is used to make it faster when running the code again by loading a stored n-gram model instead of creating it every time we run scar.py. If the corpus is changed, please delete the ngram_models.pkl file and run scar.py again to build a new model based on the corpus provided.
    - (Optional): Before running the spell checker inspect the file to pick the spell checker model and to variables that you prefer.